package com.mcprohosting.plugins.dynamicbungee.config;

import com.mcprohosting.plugins.dynamicbungee.DynamicBungee;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class MainConfig extends Config {

    public MainConfig(Plugin plugin) {
        CONFIG_HEADER = new String[]{"Dynamic Bungee Configuration!"};
        CONFIG_FILE = new File(plugin.getDataFolder(), "config.yml");

        invoke();
    }

    public void invoke() {
        try {
            this.init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public String settings_motd = "Replace Me MOTD";
    private String jedis_host = "127.0.0.1";
    private int jedis_port = 6379;
    private int jedis_timeout = 10000;
    private String jedis_password = "";

    public JedisPool getJedisPool() {
        JedisPool pool;

        FutureTask<JedisPool> task = new FutureTask<>(new Callable<JedisPool>() {
            @Override
            public JedisPool call() throws Exception {
                JedisPool pool;
                // With recent versions of Jedis, we must set the classloader to the one BungeeCord used
                // to load DynamicBungee with.
                ClassLoader previous = Thread.currentThread().getContextClassLoader();
                Thread.currentThread().setContextClassLoader(DynamicBungee.class.getClassLoader());

                JedisPoolConfig config = new JedisPoolConfig();
                config.setTestOnBorrow(true);

                if (jedis_password == null || jedis_password.equals("")) {
                    pool = new JedisPool(config, jedis_host, jedis_port, jedis_timeout);
                } else {
                    pool = new JedisPool(config, jedis_host, jedis_port, jedis_timeout, jedis_password);
                }

                // Reset classloader and return the pool
                Thread.currentThread().setContextClassLoader(previous);

                return pool;
            }
        });

        ProxyServer.getInstance().getScheduler().runAsync(DynamicBungee.getPlugin(), task);

        try {
            pool = task.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Unable to create Redis pool", e);
        }
        return pool;
    }

}
