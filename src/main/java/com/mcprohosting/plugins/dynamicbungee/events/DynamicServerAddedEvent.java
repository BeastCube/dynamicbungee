package com.mcprohosting.plugins.dynamicbungee.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Event;

@AllArgsConstructor
public class DynamicServerAddedEvent extends Event {

    @Getter
    private final String name;
    @Getter
    private final ServerInfo info;

}
